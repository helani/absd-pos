package webServalet;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet(name = "ItemServlet")
public class ItemServlet extends HttpServlet {

    DataSource pool;

    @Override
    public void init() throws ServletException {
        super.init();
        ServletContext servletContext = getServletContext();
        if (pool == null){
            pool = (DataSource) servletContext.getAttribute("pospool");
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String code = request.getParameter("code");
        String description = request.getParameter("description");
        System.out.println(description);
        int qty = Integer.parseInt(request.getParameter("qty"));
        double price = Double.parseDouble(request.getParameter("price"));
        Connection connection = null;
        PreparedStatement statement = null;


        try {
            connection = pool.getConnection();

            statement = connection.prepareStatement("INSERT INTO  item VALUES (?,?,?,?)");
            statement.setString(1,code);
            statement.setString(2,description);
            statement.setInt(3,qty);
            statement.setDouble(4,price);
            int resultSet = statement.executeUpdate();
            System.out.println(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (statement != null & connection != null){
                try {
                    statement.close();
//                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Connection connection = null;
        PreparedStatement statement = null;

        try {
            Context context = new InitialContext();
            DataSource pool = (DataSource) context.lookup("java:comp/env/pos");
            connection = pool.getConnection();

            statement = connection.prepareStatement("SELECT * FROM item");
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                String code = resultSet.getString(1);
                String name = resultSet.getString(2);
                int qty = resultSet.getInt(3);
                double price = resultSet.getDouble(4);

            }

        } catch (NamingException | SQLException e) {
            e.printStackTrace();
        }

    }


}
