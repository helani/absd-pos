package webServalet;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet(name = "OrderServlet")
public class OrderServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String orderID = request.getParameter("orderID");
        String custID = request.getParameter("custID");
        String orderDate = request.getParameter("order_date");
//        double salary = Double.parseDouble(request.getParameter("salary"));
        String itemcode=request.getParameter("itemcode");
        int quantity= Integer.parseInt(request.getParameter("quantity"));
        double price= Double.parseDouble(request.getParameter("unitPrice"));



        Connection connection = null;
        PreparedStatement statement = null;
        try {
            Context context = new InitialContext();
            DataSource pool = (DataSource) context.lookup("java:comp/env/pos");
            connection = pool.getConnection();

            statement = connection.prepareStatement("insert into orders values(?,?,?)");
            statement.setString(1,orderID);
            statement.setString(2,custID);
            statement.setString(3,orderDate);
            int resultSet = statement.executeUpdate();
            System.out.println(resultSet);
            int qun=0;

            if (resultSet==1){
                statement = connection.prepareStatement("select itemQuantity from Item where itemCode=? ");
                statement.setString(1,itemcode);
                ResultSet resultSet1 = statement.executeQuery();
                if(resultSet1.next()){
                    qun = resultSet1.getInt(1);
                }
                if (qun>quantity){
                    statement = connection.prepareStatement("insert into order_details values(?,?,?,?)");
                    statement.setString(1,orderID);
                    statement.setString(2,itemcode);
                    statement.setInt(3,quantity);
                    statement.setDouble(4,price);
                    int rst = statement.executeUpdate();

                    if (rst==1){
                        statement = connection.prepareStatement("update Item set itemQuantity=itemQuantity-? where itemcode=?");
                        statement.setInt(1,quantity);
                        statement.setString(2,itemcode);
                        int res = statement.executeUpdate();
                    }
                }
            }

        } catch (NamingException | SQLException e) {
            e.printStackTrace();
        }finally {
            if (statement != null){
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
