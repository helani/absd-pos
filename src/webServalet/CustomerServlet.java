package webServalet;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Enumeration;

@WebServlet(name = "CustomerServlet")
public class CustomerServlet extends HttpServlet {

    DataSource source;

    @Override
    public void init() throws ServletException {
        super.init();
        ServletContext servletContext = getServletContext();
        if (source == null){
            source = (DataSource) servletContext.getAttribute("pospool");
        }
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String code = request.getParameter("code");
        String name = request.getParameter("name");
        String address = request.getParameter("address");
        double salary = Double.parseDouble(request.getParameter("salary"));


        Connection connection = null;
        PreparedStatement statement = null;
        try {
            Context context = new InitialContext();
            DataSource pool = (DataSource) context.lookup("java:comp/env/pospool");
            connection = pool.getConnection();

            statement = connection.prepareStatement("insert into customer values(?,?,?,?)");
            statement.setString(1,code);
            statement.setString(2,name);
            statement.setString(3,address);
            statement.setDouble(4,salary);
            int resultSet = statement.executeUpdate();
            System.out.println(resultSet);

        } catch (NamingException | SQLException e) {
            e.printStackTrace();
        }finally {
            if (statement != null){
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Connection connection = null;
        PreparedStatement statement = null;

        PrintWriter out = response.getWriter();
        response.setContentType("text/plain");

//        out.write("[");

        try {
            Context context = new InitialContext();
            DataSource pool = (DataSource) context.lookup("java:comp/env/pos");
            connection = pool.getConnection();
            statement = connection.prepareStatement("SELECT * FROM customer");
            ResultSet resultSet =statement.executeQuery();
            while (resultSet.next()){
                String code = resultSet.getString(1);
                String name = resultSet.getString(2);
                String address = resultSet.getString(3);
                double salary = resultSet.getDouble(4);
                System.out.println(code+" "+name+" "+address+" "+salary);

                out.write("[" + code + "," + name + "," + address + "," + salary + "]");
            }

        } catch (NamingException | SQLException e) {
            e.printStackTrace();
        }finally {
            if (statement != null && connection != null){
                try {
                    statement.close();
                    connection.close();

//                    out.write("]");

                    out.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPut(req, resp);
        String code = req.getParameter("code");
        System.out.println(code);
        String name = req.getParameter("name");
        System.out.println(name+"name");
        String address = req.getParameter("address");
        String sal = req.getParameter("salary");
        System.out.println(sal);
        double salary = Double.parseDouble(sal);
        System.out.println(salary);


        PreparedStatement stm = null;
        Connection connection = null;

        try {
            Context context = new InitialContext();
            DataSource pool = (DataSource) context.lookup("java:comp/env/pos");
            connection = pool.getConnection();

            stm = connection.prepareStatement("UPDATE customer set  customerName = ?,  customerAddress = ? , customerSalary = ? where customerCode = ? ");
            stm.setString(1,name);
            stm.setString(2,address);
            stm.setDouble(3,salary);
            stm.setString(4,code);
            System.out.println(salary +"1");
            int resultSet = stm.executeUpdate();
            System.out.println(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (NamingException e) {
            e.printStackTrace();
        } finally {
            if (stm != null){
                try {
                    stm.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                };

            }
        }
    }

    @Override
    protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doOptions(req, resp);

        Enumeration<String> parameterNames = req.getParameterNames();

        while (parameterNames.hasMoreElements()) {
            String paramName = parameterNames.nextElement();
            System.out.println("Pareameter : " + req.getParameterValues(paramName));
        }

        String code = req.getParameter("code");

        System.out.println("Customer ID : " + code);

        PreparedStatement statement = null;
        Connection connection = null;

        try {
            Context context = new InitialContext();
            DataSource pool = (DataSource) context.lookup("java:comp/env/pos");
            connection = pool.getConnection();
            statement = connection.prepareStatement("DELETE FROM customer WHERE customerCode = ?");
            statement.setString(1,code);
            int resultSet = statement.executeUpdate();
            System.out.println(resultSet);

        } catch (NamingException | SQLException e) {
            e.printStackTrace();
        }finally {
            if (statement != null && connection !=null){
                try {
                    statement.close();
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
