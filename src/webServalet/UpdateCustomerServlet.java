package webServalet;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@WebServlet(name = "UpdateCustomerServlet")
public class UpdateCustomerServlet extends HttpServlet {

    DataSource source;

    @Override
    public void init() throws ServletException {
        super.init();
        ServletContext servletContext = getServletContext();
        if (source == null){
            source = (DataSource) servletContext.getAttribute("pospool");
        }
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String code = request.getParameter("code");
        String name = request.getParameter("name");
        double sal = Double.parseDouble(request.getParameter("salary"));
        String address = request.getParameter("address");

        Connection connection = null;
        PreparedStatement statement = null;
        try {
            Context context = new InitialContext();
            DataSource pool = (DataSource) context.lookup("java:comp/env/pos");
            connection = pool.getConnection();

            statement = connection.prepareStatement("UPDATE customer SET customerName = ? , customerAddress = ? , customerSalary = ? WHERE customerCode = ? ");
            statement.setString(1,name);
            statement.setString(2,address);
            statement.setDouble(3,sal);
            statement.setString(4,code);
            int resultSet = statement.executeUpdate();
            System.out.println(resultSet);

        } catch (NamingException | SQLException e) {
            e.printStackTrace();
        }finally {
            if (statement != null && connection != null){
                try {
                    statement.close();
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
