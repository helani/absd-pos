package dbc;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class DBConnection {
    public static DataSource getConnectionPool(){
        InitialContext initialContext = null;
        DataSource source = null;

        try {
            initialContext = new InitialContext();
            source = (DataSource) initialContext.lookup("java:comp/env/pospool");
        } catch (NamingException e) {
            e.printStackTrace();
        }
        return source;
    }

}
