$(document).ready(
    function () {

        /**
         * house form input fields
         */
        let $itemID = $('#itemID');
        let $itemName = $('#itemName');
        let $itemQty = $('#itemQty');
        let $unitPrice = $('#unitPrice');

        /**
         * setup house attributes
         */
        let $itemSaveBtn = $('#itemSaveBtn');
        // let $customerTable = $('#customerTable');

        /**
         * this method for set item row
         *
         * @param id
         * @param name
         * @param qty
         * @param unitPrice
         * @returns {string}
         */
        // function getCustomer(id, name, address, salary) {
        //
        //     let customerRow = "<tr>\n" +
        //         "<th scope=\"row\">" + id + "</th>\n" +
        //         "<td>" + name + "</td>\n" +
        //         "<td>" + address + "</td>\n" +
        //         "<td>" + salary + "</td>\n" +
        //         "</tr>";
        //
        //     return customerRow;
        // }

        /**
         * this method for analyse the data form response
         *
         * @param data
         */
        // function analyzeResponse(data) {
        //     for (let i in data) {
        //         let houseData = data[i];
        //
        //         let id = houseData[0];
        //         let name = houseData[1];
        //         let hPoint = houseData[2];
        //         let icon = houseData[3];
        //
        //         $customerTable.append(getCustomer(id, name, icon, hPoint));
        //     }
        // }

        /**
         * this method for load all custoemrs
         */
        // function loadAllCustomers() {
        //     $.ajax({
        //         type: "GET",
        //         url: baseURL + '/House/all',
        //         dataType: 'json'
        //     }).done(function (data) {
        //         analyzeResponse(data);
        //     });
        // }

        // loadAllCustomers();

        /**
         * this method for ajax request to save item
         */
        function saveItem() {
            $.ajax({
                url: 'http://localhost:8080/ThogakadeProject_war_exploded/item',
                type: "POST",
                data: {
                    code: $itemID.val(),
                    description: $itemName.val(),
                    qty: $itemQty.val(),
                    price: $unitPrice.val()
                },
                dataType: 'json'
            }).done(function (data) {
                alert(data);

                loadAllCustomers();
            });
        }

        // function clearText(){
        //     $customerID.val("");
        //     $customerName.val("");
        //     $customerAddress.val("");
        //     $customerSalary.val("");
        // }

        $itemSaveBtn.click(function () {
            saveItem();
            // clearText();
        });

    });
